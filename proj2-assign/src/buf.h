///////////////////////////////////////////////////////////////////////////////
/////////////  The Header File for the Buffer Manager /////////////////////////
///////////////////////////////////////////////////////////////////////////////


#ifndef BUF_H
#define BUF_H

#include "db.h"
#include "page.h"

#include "list"


#define NUMBUF 20 
// Default number of frames, artifically small number for ease of debugging.

#define HTSIZE 7
#define A 3244
#define B 5

// Hash Table size
//You should define the necessary classes and data structures for the hash table, 
// and the queues for LSR, MRU, etc.

/*******************ALL BELOW are purely local to buffer Manager********/

// You should create enums for internal errors in the buffer manager.
enum bufErrCodes  { 
    PAGE_NOT_FOUND, BUFFER_FULL, INVALID_PIN_COUNT, PAGE_PINNED,
};

class Replacer;

class HashTable {

    class HashNode {
    public:
        PageId pid;
        int bufNo;

        HashNode(PageId x, int y) {
            pid = x;
            bufNo = y;
        }
    };

private:
    list<HashNode> table[HTSIZE];

    inline int hash(PageId pid) {
        return (pid * A + B) % HTSIZE;
    }

public:
    int get(PageId pid) {
        int h = hash(pid);
        for (list<HashNode>::iterator itr = table[h].begin(); itr != table[h].end(); ++itr) {
            if (itr->pid == pid)
                return itr->bufNo;
        }
        return -1;
    }

    void put(PageId pid, int bufNo) {
        int h = hash(pid);
        table[h].push_back(HashNode(pid, bufNo));
    }

    void remove(PageId pid) {
        int h = hash(pid);
        for (list<HashNode>::iterator itr = table[h].begin(); itr != table[h].end(); ++itr) {
            if (itr->pid == pid) {
                table[h].erase(itr);
                return;
            }
        }
    }
    /*
    void debug() {
        for (int i = 0; i < HTSIZE; ++i)
        {
            for (list<HashNode>::iterator itr = table[i].begin(); itr != table[i].end(); ++itr) {
                cout << itr->pid << ' ' << itr->bufNo << endl;
            }
        }
    }
    */
};

class BufDesc {
public:
    int pageId;
    int pin_count;
    bool dirty;
    int next;
    int prev;

    BufDesc() {
        pageId = INVALID_PAGE;
        pin_count = 0;
        dirty = false;
        next = prev = -1;
    }
};

class BufMgr {

private: // fill in this area
    int size;
    BufDesc* bufDesc;
    int head;
    int tail;
    HashTable table;

    list<int> freeList;

    int getFreePage() {
        if (!freeList.empty()) {
            int ret = freeList.front();
            freeList.pop_front();
            return ret;
        }
        return head;
    }

    void remove_in_list(int i) {
        if (bufDesc[i].prev != -1)
            bufDesc[bufDesc[i].prev].next = bufDesc[i].next;
        else if (head == i)
            head = bufDesc[i].next;
        if (bufDesc[i].next != -1)
            bufDesc[bufDesc[i].next].prev = bufDesc[i].prev;
        else if (tail == i)
            tail = bufDesc[i].prev;
        bufDesc[i].prev = bufDesc[i].next = -1;
    }

    void push_to_top(int i) {
        bufDesc[i].prev = -1;
        bufDesc[i].next = head;
        if (head != -1)
            bufDesc[head].prev = i;
        head = i;
        if (tail == -1)
            tail = head;
    }

    void push_to_bottom(int i) {
        bufDesc[i].next = -1;
        bufDesc[i].prev = tail;
        if (tail != -1)
            bufDesc[tail].next = i;
        tail = i;
        if (head == -1)
            head = tail;
    }

public:
    /*
    void debug() {
        table.debug();
        cout << "free: ";
        for (list<int>::iterator itr = freeList.begin(); itr!=freeList.end(); ++itr)
        {
            cout << *itr << "->";
        }
        cout << endl;
        cout << "head: " << head << " tail: " << tail << endl;
        int x = head;
        while(x != -1) {
            cout << x << "->";
            x = bufDesc[x].next;
        }
        cout << endl;
    }
    */
    Page* bufPool; // The actual buffer pool

    BufMgr (int numbuf, Replacer *replacer = 0); 
    // Initializes a buffer manager managing "numbuf" buffers.
	// Disregard the "replacer" parameter for now. In the full 
  	// implementation of minibase, it is a pointer to an object
	// representing one of several buffer pool replacement schemes.

    ~BufMgr();           // Flush all valid dirty pages to disk

    Status pinPage(PageId PageId_in_a_DB, Page*& page, int emptyPage=0);
        // Check if this page is in buffer pool, otherwise
        // find a frame for this page, read in and pin it.
        // also write out the old page if it's dirty before reading
        // if emptyPage==TRUE, then actually no read is done to bring
        // the page

    Status unpinPage(PageId globalPageId_in_a_DB, int dirty, int hate);
        // hate should be TRUE if the page is hated and FALSE otherwise
        // if pincount>0, decrement it and if it becomes zero,
        // put it in a group of replacement candidates.
        // if pincount=0 before this call, return error.

    Status newPage(PageId& firstPageId, Page*& firstpage, int howmany=1); 
        // call DB object to allocate a run of new pages and 
        // find a frame in the buffer pool for the first page
        // and pin it. If buffer is full, ask DB to deallocate 
        // all these pages and return error

    Status freePage(PageId globalPageId); 
        // user should call this method if it needs to delete a page
        // this routine will call DB to deallocate the page 

    Status flushPage(PageId pageid);
        // Used to flush a particular page of the buffer pool to disk
        // Should call the write_page method of the DB class

    Status flushAllPages();
	// Flush all pages of the buffer pool to disk, as per flushPage.

    /* DO NOT REMOVE THIS METHOD */    
    Status unpinPage(PageId globalPageId_in_a_DB, int dirty=FALSE)
        //for backward compatibility with the libraries
    {
      return unpinPage(globalPageId_in_a_DB, dirty, FALSE);
    }
};

#endif
