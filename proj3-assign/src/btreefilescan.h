/*
 * btreefilescan.h
 *
 * sample header file
 *
 */
 
#ifndef _BTREEFILESCAN_H
#define _BTREEFILESCAN_H

#include "btfile.h"

// errors from this class should be defined in btfile.h

class BTreeFileScan : public IndexFileScan {
public:
    friend class BTreeFile;

    // get the next record
    Status get_next(RID & rid, void* keyptr);

    // delete the record currently scanned
    Status delete_current();

    int keysize(); // size of the key

    // destructor
    ~BTreeFileScan();
private:
	int keySize;
	AttrType keyType;

	Status status;
	bool init;

	void *hiKey, *curKey;

	BTLeafPage *curLeafPage;
	RID curKeyRid;
	RID curDataRid;

	bool dirty;

};

#endif
